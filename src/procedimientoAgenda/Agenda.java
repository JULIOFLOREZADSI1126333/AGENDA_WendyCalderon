
package procedimientoAgenda;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Wendy
 */
public class Agenda extends Persona {

    Persona [] ListaContactos = new Persona[100];
    private int Contactos = 0; // Contador de objetos creados. Variable muy importante.
 
     public void Consultar(String nombre, String apellido,int telefono,String direccion, String genero) {
        for (int i = 0; i < this.Contactos; i++) {
 
            if (nombre.equals(this.ListaContactos[i].getNombre())) {
                System.out.println("Ya existe un contacto con ese nombre");
            }
        }
 
    }
    
        public void Registrar (String nombre,String apellido,String direccion,String genero, int telefono) {
     
          if (Contactos < 100) {
            this.ListaContactos[Contactos] = new Persona();
            this.ListaContactos[Contactos].set_nombre(nombre);
            this.ListaContactos[Contactos].set_apellido(apellido);
            this.ListaContactos[Contactos].set_telefono(telefono);
            this.ListaContactos[Contactos].set_direccion(direccion);
            this.ListaContactos[Contactos].set_genero(genero);
            this.Contactos++;
            Ordenar();
        } else {
            System.out.println("No hay espacio en la memoria de la agenda");
        }
 
    }
    
        
    public void Buscar(String nombre) {
        boolean encontrado = false;
 
        for (int i = 0; i < Contactos; i++) {
            if (nombre.equals(this.ListaContactos[i].getNombre())) {
                System.out.println(this.ListaContactos[i].getNombre() + "-" + this.ListaContactos[i].getApellido() +"-" + this.ListaContactos[i].getDireccion() +"-" + this.ListaContactos[i].getGenero() +"-" + "Telefono:" + this.ListaContactos[i].getTelefono());
                encontrado = true;
            }
        }
        if (!encontrado) {
            System.out.println("No existe contacto");
        }
    }
    
     public void Ordenar() {
        //Este método ordenará el array de contacos por el nombre mediante el Método Burbuja
        int N = this.Contactos;
        String nombre1;
        String nombre2;
        //Optimizo para cuando tenga más de dos elementos al menos.
        if (Contactos >= 2) {
            for (int i = 1; i <= N - 1; i++) {
                for (int j = 1; j <= N - i; j++) {
                    nombre1 = this.ListaContactos[j - 1].getNombre();
                    nombre2 = this.ListaContactos[j].getNombre();
                    if (nombre1.charAt(0) > nombre2.charAt(0)) {
                        Persona tmp = this.ListaContactos[j - 1];
                        this.ListaContactos[j - 1] = this.ListaContactos[j];
                        this.ListaContactos[j] = tmp;
                    }
                }
            }
        }
    }
 
    public void Eliminar() {
         try {
            boolean encontrado = false;
            BufferedReader leer = new BufferedReader(new InputStreamReader(System.in));
            System.out.println(" Ingresa el nombre que deseas eliminar:");
            String eliminar = leer.readLine().toUpperCase();
            if (Contactos == 0) {
                System.out.println("No hay contactos");
            } else {
                for (int i = 0; i < Contactos; i++) {
 
                    if (eliminar.equals(this.ListaContactos[i].getNombre())) {
                        System.out.println(i + 1 + " " + this.ListaContactos[i].getNombre() + "-" + "Telefono : " + this.ListaContactos[i].getTelefono());
                        encontrado = true;
                    }
                }
                if (encontrado) {
                    System.out.println("ingresa el  numero del contacto que quieres eliminar");
                    int eliminarNumero = Integer.parseInt(leer.readLine());
                    eliminarNumero--;
                    System.out.println("¿totalmente seguro?");
                    String respuesta;
                    respuesta = leer.readLine();
                    respuesta = respuesta.toUpperCase();
                    if (respuesta.equals("S")) {
                        Persona[] temporal = new Persona[99];
                        int ii = 0;
                        boolean encontrado2=false;
                        for (int i = 0; i < this.Contactos; i++) {
 
                            if (i != eliminarNumero) {
                                // Creo el objeto temporal para el borrado
                                if (!encontrado2)
                                {
                                  temporal[ii] = this.ListaContactos[ii];
                                  ii++;
                                }
                                else
                                {
                                    if (ii<this.Contactos)
                                    { temporal[ii] = this.ListaContactos[ii+1];
                                     ii++;
                                    }
                                }
 
                            } else {
                                temporal[ii] = this.ListaContactos[ii + 1];
                                ii++;
                                encontrado2=true;
 
                            }
                        }
                        this.Contactos--;
                        System.out.println("Se ha eliminado el contacto");
                        for (int j = 0; j < this.Contactos; j++) {
                            this.ListaContactos[j] = temporal[j];
 
                        }
 
                    }
 
                } else {
                    System.out.println("No se ha encontrado el nombre");
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(Agenda.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
 
     
        public void Modificar() {
           try {
            boolean encontrado = false;
            BufferedReader leer = new BufferedReader(new InputStreamReader(System.in));
            System.out.println(" ingresa  el nombre que deseas modificar");
            String eliminar = leer.readLine().toUpperCase();
            if (Contactos == 0) {
                System.out.println("No hay contactos");
            } else {
                for (int i = 0; i < this.Contactos; i++) {
 
                    if (eliminar.equals(this.ListaContactos[i].getNombre())) {
                        System.out.println( i + 1 + "-" + this.ListaContactos[i].getNombre() + "- " + this.ListaContactos[i].getApellido() + "-" + "-" + this.ListaContactos[i].getTelefono());
                        encontrado = true;
                    }
                }
                if (encontrado) {
                    System.out.println(" Ingrese el numero que desea modificar");
                    int modificarNumero = Integer.parseInt(leer.readLine());
 
                    System.out.println("ingresa el nombre");
                    String nombreNuevo = leer.readLine();
                    System.out.println("Ingresa el apellido:");
                    String apellidoNuevo = leer.readLine();
                    System.out.println("ingresa la direccion");
                    String direccionNueva = leer.readLine();
                    System.out.println("Ingresa el genero:");
                    String generoNuevo = leer.readLine();
                    System.out.println("Ingresa el telefono");
                    Integer telefonoNuevo = Integer.parseInt(leer.readLine());
                    
                    this.ListaContactos[modificarNumero - 1].set_nombre(nombreNuevo);
                    this.ListaContactos[modificarNumero - 1].set_apellido(apellidoNuevo);
                    this.ListaContactos[modificarNumero - 1].set_direccion(direccionNueva);
                    this.ListaContactos[modificarNumero - 1].set_genero(generoNuevo);
                    this.ListaContactos[modificarNumero - 1].set_telefono(telefonoNuevo);
 
                   Ordenar();
                } else {
                    System.out.println("No existe ese nombre en tus contactos");
                }
 
            }
        
       } catch (IOException ex) {
            Logger.getLogger(Agenda.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
 public void Caracteres(){
     for (int i=0;i<Contactos;i++){
         
     }
 }
}
